
# Define required providers
terraform {
  required_version = ">= 0.14.0"
    required_providers {
	  gitlab = {
        source  = "gitlabhq/gitlab"
  		version = ">=15.10.0"
	}
  }
}

