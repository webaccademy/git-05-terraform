generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "gitlab" {
  token = var.GITLAB_TOKEN
  base_url = var.gitlab_url
}
EOF
}

generate "backend" {
  path = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  backend "http" {
	address = "${get_env("TF_VAR_GITLAB_URL")}"
  }
}
EOF
}

