<!-- BEGIN_TF_DOCS -->
## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (>= 0.14.0)

## Providers

The following providers are used by this module:

- <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) (15.10.0)

## Modules

No modules.

## Resources

The following resources are used by this module:

- [gitlab_branch.protected_branch](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch) (resource)
- [gitlab_branch_protection.protected_branch](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) (resource)
- [gitlab_deploy_token.sample_deploy_token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_token) (resource)
- [gitlab_group.sample_group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group) (resource)
- [gitlab_group_access_token.sample_group_token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_access_token) (resource)
- [gitlab_group_variable.sample_group_variable](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) (resource)
- [gitlab_project.sample_project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) (resource)
- [gitlab_project_access_token.sample_project_access_token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_access_token) (resource)

## Required Inputs

The following input variables are required:

### <a name="input_GITLAB_TOKEN"></a> [GITLAB\_TOKEN](#input\_GITLAB\_TOKEN)

Description: env TF\_VAR\_GITLAB\_TOKEN

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url)

Description: n/a

Type: `string`

Default: `"https://darkman-git.os.lailio.cloud/api/v4"`

## Outputs

No outputs.
<!-- END_TF_DOCS -->