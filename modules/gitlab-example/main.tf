
# Add a group
resource "gitlab_group" "sample_group" {
  name        = "sample_group"
  path        = "sample_group"
  description = "An example group"
}

# Add a project owned by the user
resource "gitlab_project" "sample_project" {
  name = "sample_project"
  namespace_id = gitlab_group.sample_group.id
  depends_on = [gitlab_group.sample_group]
# GitLab EE required
#  approvals_before_merge = 2
#  push_rules {
#    reject_unsigned_commits        = true
#	prevent_secrets        = true
#  }
}

# Group access token
resource "gitlab_group_access_token" "sample_group_token" {
  group        = "sample_group"
  name         = "Example group access token"
  expires_at   = "2024-03-14"
  access_level = "developer"

  scopes = ["api"]
  depends_on = [gitlab_group.sample_group]
}

# Deploy token
resource "gitlab_deploy_token" "sample_deploy_token" {
  project    = "sample_group/sample_project"
  name       = "Example deploy token"
  username   = "deploy-username"
  expires_at = "2024-03-14T00:00:00.000Z"

  depends_on = [gitlab_project.sample_project]
  scopes = ["read_repository", "read_registry"]
}

# Group variable
resource "gitlab_group_variable" "sample_group_variable" {
  group             = "sample_group"
  key               = "group_variable_key"
  value             = "group_variable_value"
  protected         = false
  masked            = false
  environment_scope = "*"

  depends_on = [gitlab_group.sample_group]
}

resource "gitlab_project_access_token" "sample_project_access_token" {
  project      = "sample_group/sample_project"
  name         = "Example project access token"
  expires_at   = "2024-03-14"
  access_level = "reporter"

  scopes = ["api"]
  depends_on = [gitlab_project.sample_project]
}

# Branch
resource "gitlab_branch" "protected_branch" {
  name    = "protected_branch"
  ref     = "main"
  project = gitlab_project.sample_project.id
  depends_on = [gitlab_project.sample_project]
}

# Branch protection
resource "gitlab_branch_protection" "protected_branch" {
  project                = "sample_group/sample_project"
  branch                 = "protected_branch"
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
  allow_force_push       = false

  dynamic "allowed_to_push" {
    for_each = [1]
    content {
      user_id = allowed_to_push.value
    }
  }
  depends_on = [gitlab_branch.protected_branch]
}
