#####################################################################################

variable "gitlab_token" { 
	type = string 
	sensitive = true
}

variable gitlab_url {
	type = string
	default = "https://darkman-git.os.lailio.cloud/api/v4"
}
