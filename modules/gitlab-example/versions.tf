
# Define required providers
terraform {
  required_version = ">= 0.14.0"
    required_providers {
	  gitlab = {
        source  = "gitlabhq/gitlab"
  		version = ">=15.10.0"
	}
  }
}

provider "gitlab" {
  token = var.gitlab_token
  base_url = var.gitlab_url
}

