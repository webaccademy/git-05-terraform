module "gitlab-example" {
	source = "./modules/gitlab-example"
	gitlab_url = var.GITLAB_URL
	gitlab_token = var.GITLAB_TOKEN
}
