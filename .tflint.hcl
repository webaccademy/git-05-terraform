config {
  format = "compact"

  module = true
  force = false
  disabled_by_default = false

}

plugin "terraform" {
  enabled = true
  preset  = "recommended"
  version = "0.2.2"
  source  = "github.com/terraform-linters/tflint-ruleset-terraform"
}

