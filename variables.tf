#####################################################################################

# env TF_VAR_GITLAB_TOKEN
variable "GITLAB_TOKEN" { 
	type = string 
	sensitive = true
}

variable GITLAB_URL {
	type = string
	#default = "https://darkman-git.os.lailio.cloud/api/v4"
}
